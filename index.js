const { GraphQLServer } = require("graphql-yoga");
const { prisma } = require("./server/src/generated/prisma-client");
const cors = require("cors");
require("dotenv").config();
const path = require("path");
const express = require("express");
const options = {
  port: 4000,
  endpoint: "/graphql",
  subscriptions: "/subscriptions",
  playground: "/playground"
};

/* Resolvers */
const Query = require("./server/src/resolvers/Query");
const Mutation = require("./server/src/resolvers/Mutation");
const User = require("./server/src/resolvers/User");
const Listing = require("./server/src/resolvers/Listing");
const Rating = require("./server/src/resolvers/Rating");
const Category = require("./server/src/resolvers/Category");
const ShoppingCart = require("./server/src/resolvers/ShoppingCart");
const OrderSection = require("./server/src/resolvers/OrderSection");
const ShoppingCartItem = require("./server/src/resolvers/ShoppingCartItem");
const OrderItem = require("./server/src/resolvers/OrderItem");
/* Resolver end */

const resolvers = {
  Query,
  Mutation,
  User,
  Listing,
  Rating,
  Category,
  ShoppingCart,
  OrderSection,
  ShoppingCartItem,
  OrderItem
};

const server = new GraphQLServer({
  typeDefs: "./server/src/schema.graphql",
  resolvers,
  context: request => {
    return {
      ...request,
      prisma
    };
  }
});
server.express.use("/*", cors()); // allow cors
server.express.use(express.static("./frontend/dist"));

// STRIPE WEBHOOK
const addWebhook = require("./server/src/resolvers/webhook.js");
// var stripe = Stripe("pk_test_ShebKpfmrPLYHqRTwBMn52X70098H7IycL"); //public key
addWebhook(server.express);

server.start(options, ({ port }) =>
  console.log(
    ` Website is running on http://localhost:${port} \n GraphQL playground is on http://localhost:${port}/playground `
  )
);
