import Vue from "vue";
import Router from "vue-router";
import Home from "./views/Home.vue";
import Cart from "./views/Cart.vue";
import SearchDisplay from "./views/SearchDisplay.vue";
import About from "./views/About.vue";
import Orders from "./views/Orders.vue";
import SingleItem from "./views/SingleItem.vue";
import Upload from "./views/Upload.vue";
import Login from "./views/Login.vue";
import Account from "./views/Account.vue";
import Signup from "./views/Signup.vue";
import Categories from "./views/Categories.vue";


Vue.use(Router);

export default new Router({
  mode: "history",
  base: process.env.BASE_URL,
  routes: [
    {
      path: "/",
      name: "home",
      component: Home,
      props: (route) => ({ query: route.query.success_id }) 
    },
    {
      path: "/cart",
      name: "cart",
      component: Cart
    },
    { path: '/search', 
      component: SearchDisplay, 
      props: (route) => ({ query: route.query.q }) 
    },
    {
      path: "/category/:categoryName",
      component: SearchDisplay
    },
    {
      path: "/orders",
      name: "Orders",
      component: Orders
    },
    {
      path: "/about",
      name: "about",
      component: About
    },
    {
      path: "/single-item",
      name: "singleItem",
      component: SingleItem,
      props: (route) => ({ query: route.query.stuff }) 
    },
    {
      path: "/upload",
      name: "Upload",
      component: Upload
    },
    {
      path: "/login",
      name: "Login",
      component: Login,
      props: (route) => ({ query: route.query.something }) 
    },
    {
      path: "/account",
      name: "Account",
      component: Account
    },
    {
      path: "/signup",
      name: "Signup",
      component: Signup
    },
    {
      path: "/categories",
      name: "Categories",
      component: Categories
    }
  ]
});
