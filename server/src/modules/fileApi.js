const uuid = require("uuid/v1");
const aws = require("aws-sdk");
require("dotenv").config();

const s3 = new aws.S3({
  accessKeyId: process.env.S3_ACCESS_KEY_ID,
  secretAccessKey: process.env.S3_SECRET_ACCESS_KEY,
  params: {
    Bucket: process.env.S3_TEST_BUCKET
  },
  region: "us-east-1",
  credentials: new aws.Credentials({
    accessKeyId: process.env.S3_ACCESS_KEY_ID,
    secretAccessKey: process.env.S3_SECRET_ACCESS_KEY,
    sessionToken: null
  })
});

exports.processUpload = async (upload, context) => {
  if (!upload) {
    return console.log("ERROR: No file received.");
  }
  const { createReadStream, filename, mimetype, encoding } = await upload;
  const stream = createReadStream();
  const key = uuid() + "-" + filename;
  // ............
  var uploadParams = { Bucket: process.env.S3_TEST_BUCKET, Key: "", Body: "", ContentType: mimetype };
  uploadParams.Body = stream;
  var path = require("path");
  uploadParams.Key = path.basename(key);

  // Upload to S3
  const response = await s3.upload(uploadParams).promise();

  const url = response.Location;

  const file = await context.prisma.createFile({
    filename,
    mimetype,
    encoding,
    url
  }); // same as url: url

  console.log("saved prisma file:");
  console.log(file);

  return file;
};
