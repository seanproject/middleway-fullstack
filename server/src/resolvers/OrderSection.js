function user(parent, args, context) {
  return context.prisma.orderSection({ id: parent.id }).user();
}

function orderListings(parent, args, context) {
  return context.prisma.orderSection({ id: parent.id }).orderListings();
}

module.exports = {
  user,
  orderListings
};
