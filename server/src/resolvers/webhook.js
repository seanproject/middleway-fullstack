const { prisma } = require("../generated/prisma-client");


async function handleCheckoutSession(session) {
  // empty user's shopping cart and add order to order
  console.log("inside the handler");
  console.log(session);
  console.log(session.client_reference_id);
  const userId = session.client_reference_id;
  //get user's cart to create an order
  const fragment = `
    fragment shoppingCart on User {
      shoppingCart{
        subTotal
        itemCount
        items{
          quantity
          item{
            id
            title
            description
            price
            quantity
            itemPictures{
              url
            }
          }
        }
      }
    }`;

  const cartInfo = await prisma.user({ id: userId }).$fragment(fragment);
    
  // create a new orderItems and  calculate subtotal of order
  var subtotal=cartInfo.shoppingCart.subtotal;
  const order_item_ids = [];
  for(const cartItem of cartInfo.shoppingCart.items){ // IMPORTANT NOTE: we assume the current shoppingCartItem corresponds to the stripe checkout session item
    const id = await prisma.createOrderItem({         //                 we need to validate the correspondence between the stripe checkout session and shopping cart
      quantity: cartItem.quantity,                    //                  (this won't cause any problems for a user using the website properly) 
      item: {
        connect:{
          id: cartItem.item.id
        }
      }
    }).id();
    order_item_ids.push({id: id})
  }
  stripe.paymentIntents.retrieve( // payment intent is retrieved only to get shipping address 
    session.payment_intent,
    async function(err, paymentIntent) {
      // asynchronously called
      if(err)console.log(err);
      else{
        const address = JSON.stringify(paymentIntent.shipping.address);
        // now create order section
        await prisma.createOrderSection({
          totalOrders: 1, // WHAT NUMBER DO I PUT FOR TOTAL ORDERS????
          shippingAddress: address,
          totalPrice: subtotal,
          status: "ACTIVE_PLACED", // not sure if enum is entered as a string
          user:{
            connect:{
              id: userId
            }
          }
        });
      }
    }
  );

  

}

module.exports = async function addWebhook(app) {
  const stripe = require("stripe")(
    "sk_test_FdZSKPbB92cugioz6jHX1spZ00nHMOWmak"
  );
  
  // Find your endpoint's secret in your Dashboard's webhook settings
  const endpointSecret = "whsec_HsGtFuOBTMyvLUBPJPPiclunRIQzaP9K";

  // Use body-parser to retrieve the raw body as a buffer
  const bodyParser = require("body-parser");

  // Match the raw body to content type application/json
  app.post(
    "/webhook",
    bodyParser.raw({ type: "application/json" }),
    (request, response) => {
      const sig = request.headers["stripe-signature"];

      let event;

      try {
        event = stripe.webhooks.constructEvent(
          request.body,
          sig,
          endpointSecret
        );
      } catch (err) {
        return response.status(400).send(`Webhook Error: ${err.message}`);
      }

      // Handle the checkout.session.completed event
      if (event.type === "checkout.session.completed") {
        const session = event.data.object;

        // Fulfill the purchase...
        handleCheckoutSession(session);
      }

      // Return a response to acknowledge receipt of the event
      response.json({ received: true });
    }
  );
}


