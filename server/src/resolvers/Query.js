const { getUserId } = require("../utils");

function file(parent, { id }, context, info) {
  return context.prisma.file({ where: { id } }, info);
}

function files(parent, args, context, info) {
  return context.prisma.files(args, info);
}

// return all user info
async function user(parent, args, context, info) {
  const userId = getUserId(context);
  return await context.prisma.user({ id: userId });
}

async function listingFeed(parent, args, context, info) {
  let where = {}
  if (args.id){
    where = { id: args.id }

  }
  else if (args.filter){
    where = {
      OR: [
        { description_contains: args.filter },
        { title_contains: args.filter }
      ]
    }
  } else if (args.category){
    where = {
      category: { title: args.category }
    }
  }

  const listings = await context.prisma.listings({
    where,
    skip: args.skip,
    first: args.first,
    orderBy: args.orderBy
  });
  const count = await context.prisma
    .listingsConnection({
      where
    })
    .aggregate()
    .count();
  return {
    listings,
    count
  };
}

function categoryList(parent, args, context, info) {
  return context.prisma.categories({
    skip: args.skip,
    first: args.first
  });
}

async function listingsByCategory(parent, args, context, info) {
  const where = {
    category: { title: args.category }
  }
  const listings = await context.prisma.listings({
    where,
    skip: args.skip,
    first: args.first,
    orderBy: args.orderBy
  });
  const count = await context.prisma
    .listingsConnection({
      where
    })
    .aggregate()
    .count();
  return {
    listings,
    count
  };
}

module.exports = {
  listingFeed,
  categoryList,
  file,
  files,
  user,
  listingsByCategory
};
