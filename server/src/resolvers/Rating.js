function listing(parent, args, context) {
  return context.prisma.rating({ id: parent.id }).link();
}

function user(parent, args, context) {
  return context.prisma.rating({ id: parent.id }).user();
}

module.exports = {
  listing,
  user
};
