function item(parent, args, context) {
    return context.prisma.orderItem({ id: parent.id }).item();
  }
  function quantity(parent, args, context) {
    return context.prisma.orderItem({ id: parent.id }).quantity();
  }
  
  
  module.exports = {
      item,
      quantity
  }