function listings(parent, args, context) {
  return context.prisma.user({ id: parent.id }).listings();
}

function shoppingCart(parent, args, context){
  return context.prisma.user({ id: parent.id }).shoppingCart();
}

function orderList(parent, args, context){
  return context.prisma.user({ id: parent.id }).orderList();
}

function profilePicture(parent, args, context){
  return context.prisma.user({ id: parent.id }).profilePicture();
}

function company(parent, args, context){
  return context.prisma.user({ id: parent.id }).company();
}


module.exports = {
  listings,
  shoppingCart,
  orderList,
  profilePicture,
  company
};
