function items(parent, args, context) {
  return context.prisma.shoppingCart({ id: parent.id }).items();
}

function user(parent, args, context) {
  return context.prisma.shoppingCart({ id: parent.id }).user();
}


module.exports = {
  items,
  user
};
