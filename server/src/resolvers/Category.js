function subCategories(parent, args, context) {
  return context.prisma.category({ id: parent.id }).subCategories();
}
function image(parent, args, context) {
  return context.prisma.category({ id: parent.id }).image();
}

module.exports = {
  subCategories,
  image
};
