function item(parent, args, context) {
    return context.prisma.shoppingCartItem({ id: parent.id }).item();
  }
  function quantity(parent, args, context) {
    return context.prisma.shoppingCartItem({ id: parent.id }).quantity();
  }
  
  
  module.exports = {
      item,
      quantity
  }