function postedBy(parent, args, context) {
  return context.prisma.listing({ id: parent.id }).postedBy();
}

function ratingCount(parent, args, context) {
  return context.prisma.listing({ id: parent.id }).ratings();
}

function category(parent, args, context) {
  return context.prisma.listing({ id: parent.id }).category();
}

function itemPictures(parent, args, context){
  return context.prisma.listing({ id: parent.id }).itemPictures();
}

module.exports = {
  postedBy,
  ratingCount,
  category,
  itemPictures
};
