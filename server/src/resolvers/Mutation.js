const bcrypt = require("bcryptjs");
const jwt = require("jsonwebtoken");
const { getUserId } = require("../utils");
const { processUpload } = require("../modules/fileApi");
async function signup(parent, args, context, info) {
  // validate email, password, and names
  const password = await bcrypt.hash(args.password, 12);
  const user = await context.prisma.createUser({ ...args, password });
  args.shoppingCart = await context.prisma.createShoppingCart({
    user: { connect: { id: user.id } },
    itemCount: 0,
    subTotal: 0.0
  });
  const token = jwt.sign({ userId: user.id }, process.env.APP_SECRET);
  return {
    token,
    user
  };
}

async function login(parent, args, context, info) {
  const user = await context.prisma.user({ email: args.email });
  if (!user) {
    throw new Error("No such user found");
  }
  const valid = await bcrypt.compare(args.password, user.password);
  if (!valid) {
    throw new Error("Invalid password");
  }
  const token = jwt.sign({ userId: user.id }, process.env.APP_SECRET);
  return {
    token,
    user
  };
}

async function postListing(parent, args, context, info) {
  const userId = getUserId(context);
  const fragment = `
fragment SellerId on User {
  company{
    id
  }
}`;
  const sellerId = await context.prisma
    .user({ id: userId })
    .$fragment(fragment)
    .company()
    .id();
  // arg.file is a list of id
  const files = args.pictureId.map(function(val) {
    const obj = { id: val };
    return obj;
  });
  const price = parseFloat(args.price.toFixed(2));
  const listing = await context.prisma
    .createListing({
      title: args.title,
      description: args.description,
      price: price,
      quantity: args.quantity,
      country: args.country,
      postalCode: args.postalCode,
      address: args.address,
      minQuantity: args.minQuantity,
      postedBy: { connect: { id: sellerId } },
      category: { connect: { title: args.category } }, // just make category into enum??
      itemPictures: { connect: files }
    })
    .id();
  await context.prisma.updateUser({
    where: { id: userId },
    data: { listings: { connect: { id: listing } } }
  }); //update user.listings,
  return "oh yeah, it's all coming together :)";
}

//  args.updateImage is a bool
async function updateListing(parent, args, context, info) {
  const userId = getUserId(context);
  args.imageFiles = args.imageFiles || [];
  args.imageIDs = args.imageIDs || [];
  const allFiles = await Promise.all(
    args.imageFiles.map(file => processUpload(file, context))
  );
  const fileIDs = allFiles.map(val => ({ id: val.id }));
  const delIDs = args.imageIDs.map(val => ({ id: val }));
  const price = parseFloat(args.price.toFixed(2));
  await context.prisma.updateUser({
    where: {
      id: userId
    },
    data: {
      listings: {
        update: {
          where: {
            id: args.listingId
          },
          data: {
            title: args.title,
            description: args.description,
            price: price,
            quantity: args.quantity,
            itemPictures: {
              connect: fileIDs,
              delete: delIDs
            }
          }
        }
      }
    }
  });
  return "nice";
}

async function rateListing(parent, args, context, info) {
  const userId = getUserId(context);
  const listingExists = await context.prisma.$exists.rating({
    user: { id: userId },
    listing: { id: args.listingId }
  });
  if (listingExists) {
    throw new Error(`Already rated that listing: ${args.listingId}`);
  }

  return context.prisma.createRating({
    user: { connect: { id: userId } },
    listing: { connect: { id: args.listingId } },
    value: args.value,
    review: args.review
  });
}

function createCategory(parent, args, context, info) {
  return context.prisma.createCategory({
    title: args.title,
    description: args.description
  });
}

async function uploadFile(parent, { file }, context, info) {
  return await processUpload(await file, context);
}

async function uploadFiles(parent, { files }, context, info) {
  return Promise.all(files.map(file => processUpload(file, context)));
}

async function renameFile(parent, { id, name }, context, info) {
  return context.prisma.updateFile({ data: { name }, where: { id } }, info);
}

async function deleteFile(parent, { id }, context, info) {
  return await context.prisma.deleteFile({ where: { id } }, info);
}

//add a new item to cart and update subtotal and itemCount, and make sure item is not already in cart
async function updateCart(parent, args, context, info) {
  const userId = getUserId(context);
  //check if item is already in cart
  const alread_exist = await context.prisma.$exists.user({
    AND: [
      { id: userId },
      {
        shoppingCart: {
          items_some: {
            item: {
              id: args.listing
            }
          }
        }
      }
    ]
  });
  if (alread_exist) return "duplicate";
  const fragment = `
    fragment shoppingCart on User {
      shoppingCart{
        subTotal
        itemCount
      }
    }`;
  const user = await context.prisma.user({ id: userId }).$fragment(fragment);
  const subtotal = user.shoppingCart.subTotal;
  const itemCount = user.shoppingCart.itemCount;
  const new_itemCount = itemCount + args.quantity;
  const item_price = await context.prisma.listing({ id: args.listing }).price();
  const new_subtotal = subtotal + item_price * args.quantity;
  const subt = parseFloat(new_subtotal.toFixed(2));
  await context.prisma.updateUser({
    where: { id: userId },
    data: {
      shoppingCart: {
        update: {
          subTotal: subt,
          itemCount: new_itemCount,
          items: {
            create: {
              item: { connect: { id: args.listing } },
              quantity: args.quantity
            }
          }
        }
      }
    }
  });
  return "ok";
}

//delete cart item and update subtotal
async function deleteCartItem(parent, args, context, info) {
  const userId = getUserId(context);
  const fragment = `
  fragment Cart_item on shoppingCartItem {
    quantity
    item{
      price
    }
  }
  `;
  const cart_item = await context.prisma
    .shoppingCartItem({
      id: args.itemId
    })
    .$fragment(fragment);
  //get subtotal
  const fragment2 = `
    fragment shoppingCart on User {
      shoppingCart{
        subTotal
        itemCount
      }
    }`;
  const user = await context.prisma.user({ id: userId }).$fragment(fragment2);
  const subtotal = user.shoppingCart.subTotal;
  const itemCount = user.shoppingCart.itemCount;
  const new_itemCount = itemCount - cart_item.quantity;
  const newSubtotal = subtotal - cart_item.quantity * cart_item.item.price;
  const subt = parseFloat(newSubtotal.toFixed(2));
  const temp = await context.prisma.updateUser({
    where: { id: userId },
    data: {
      shoppingCart: {
        update: {
          subTotal: subt,
          itemCount: new_itemCount,
          items: {
            delete: {
              id: args.itemId
            }
          }
        }
      }
    }
  });
  return "ok";
}

//change quantity of cart item
async function updateCartItem(parent, args, context, info) {
  const userId = getUserId(context);
  const fragment2 = `
    fragment shoppingCart on User {
      shoppingCart{
        subTotal
        itemCount
      }
    }`;
  const user = await context.prisma.user({ id: userId }).$fragment(fragment2);
  const subtotal = user.shoppingCart.subTotal;
  const itemCount = user.shoppingCart.itemCount;
  const fragment = `
    fragment Cart_item on shoppingCartItem {
      quantity
      item{
        price
      }
    }
    `;
  const old_item = await context.prisma
    .shoppingCartItem({ id: args.itemId })
    .$fragment(fragment);
  const new_subtotal =
    subtotal -
    old_item.quantity * old_item.item.price +
    args.newQuantity * old_item.item.price;
  const new_itemCount = itemCount - old_item.quantity + args.newQuantity;
  const subt = parseFloat(new_subtotal.toFixed(2));
  await context.prisma.updateUser({
    where: { id: userId },
    data: {
      shoppingCart: {
        update: {
          subTotal: subt,
          itemCount: new_itemCount,
          items: {
            update: {
              where: { id: args.itemId },
              data: {
                quantity: args.newQuantity
              }
            }
          }
        }
      }
    }
  });
  return "ok";
}

async function changeEmail(parent, args, context, info) {
  const userId = getUserId(context);
  await context.prisma.updateUser({
    where: { id: getUserId },
    data: {
      email: args.newEmail
    }
  });
  return "ok";
}

async function updateAccountInfo(parent, args, context, info) {
  const userId = getUserId(context);
  return await context.prisma.updateUser({
    where: { id: userId },
    data: {
      firstName: args.firstName,
      middleName: args.middleName,
      lastName: args.lastName,
      company: {
        update: {
          companyName: args.companyName,
          companyWebsite: args.companyWebsite
        }
      }
    }
  });
}

async function changePassword(parent, args, context, info) {
  const userId = getUserId(context);
  const password = await bcrypt.hash(args.newPassword, 10);
  return await context.prisma.updateUser({
    where: { id: userId },
    data: {
      password: password
    }
  });
}

async function createCompany(parent, args, context, info) {
  const userId = getUserId(context);
  return await context.prisma.updateUser({
    where: {
      id: userId
    },
    data: {
      company: {
        create: {
          companyName: args.companyName,
          companyWebsite: args.companyWebsite
        }
      }
    }
  });
}

//
async function checkOut(parent, args, context, info) {
  const userId = getUserId(context);
  // Set your secret key: remember to change this to your live secret key in production
  // See your keys here: https://dashboard.stripe.com/account/apikeys
  const stripe = require("stripe")(
    "sk_test_FdZSKPbB92cugioz6jHX1spZ00nHMOWmak"
  );
  const fragment = `
    fragment shoppingCart on User {
      shoppingCart{
        subTotal
        itemCount
        items{
          quantity
          item{
            title
            description
            price
            quantity
            itemPictures{
              url
            }
          }
        }
      }
    }`;

  const temp = await context.prisma.user({ id: userId }).$fragment(fragment);
 
  const lineItems = [];
  for (const cartItem of temp.shoppingCart.items) {
    lineItems.push({
      name: cartItem.item.title,
      description: cartItem.item.description,
      images: cartItem.item.itemPictures.map((val) => (val.url)),
      amount: Math.round(cartItem.item.price * cartItem.quantity * 100),
      currency: "cad",
      quantity: cartItem.quantity
    });
     // validate shoppingCart, make sure there is enough of quantity
  if(cartItem.quantity>cartItem.item.quantity){
    throw new Error("not enough items in stock");
  }
  }

  //update the left in stock quantity of each item
  for( const cartItem of temp.shoppingCart.items){
    const old_quantity = cartItem.item.quantity;
    const new_quantity = old_quantity-cartItem.quantity;
    await prisma.updateListing({
      where:{id: cartItem.item.id},
      data:{
        quantity: new_quantity
      }
    });
  }


  var id;

  const session = await stripe.checkout.sessions.create({
    billing_address_collection: 'required',
    payment_method_types: ["card"],
    client_reference_id: userId, // when webhook is received from stripe we use this field to update the user's cart/orders 
    line_items: lineItems,
    success_url:
      "http://middlway.com/?success_id=yes",
    cancel_url: "http://middlway.com/?success_id=no"
  });

  return session.id;
}

async function deleteListing(parent,args,context,info){
  const userId = getUserId(context);
  // delete product
  await context.prisma.updateUser({
    where: {id: userId},
    data:{
      listings:{
        delete:[
          {
            id: args.listingId
          }
        ]
      }
    }
  });
  return "mmm very nice";
}
module.exports = {
  signup,
  login,
  postListing,
  rateListing,
  createCategory,
  uploadFile,
  uploadFiles,
  renameFile,
  deleteFile,
  updateCart,
  deleteCartItem,
  updateCartItem,
  changeEmail,
  updateAccountInfo,
  updateListing,
  changePassword,
  createCompany,
  checkOut,
  deleteListing
};
